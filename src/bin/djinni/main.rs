use anyhow::Result;
use clap::{Args, Parser};
use serde::{Deserialize, Deserializer};

use std::{
    fmt::{self, Display},
    path::PathBuf,
};

pub(crate) trait Command {
    fn run(&self) -> Result<()>;
}

#[derive(Debug)]
enum Set {
    CommanderTheBrothersWar,
    DominariaRemastered,
    ModernHorizons2,
    RavnicaRemastered,
    TimeSpiralRemastered,
    Unknown,
}

impl<'de> Deserialize<'de> for Set {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        let set = String::deserialize(deserializer)?;
        match set.as_str() {
            "brc" => Ok(Self::CommanderTheBrothersWar),
            "dmr" => Ok(Self::DominariaRemastered),
            "mh2" => Ok(Self::ModernHorizons2),
            "rvr" => Ok(Self::RavnicaRemastered),
            "tsr" => Ok(Self::TimeSpiralRemastered),
            _ => Ok(Self::Unknown),
        }
    }
}

impl Display for Set {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Set::CommanderTheBrothersWar => write!(f, "Commander: The Brothers' War"),
            Set::DominariaRemastered => write!(f, "Dominaria Remastered: Extras"),
            Set::ModernHorizons2 => write!(f, "Modern Horizons 2: Extras"),
            Set::RavnicaRemastered => write!(f, "Ravnica Remastered: Extras"),
            Set::TimeSpiralRemastered => write!(f, "Time Spiral Remastered: Extras"),
            Set::Unknown => write!(f, "Unknown"),
        }
    }
}

#[derive(Debug, serde::Deserialize)]
pub struct Card {
    maybeboard: bool,
    name: String,
    Set: Set,

    status: String,
}

#[derive(Args)]
pub(crate) struct Ask {
    /// Path to the cube.
    cube: PathBuf,
}

fn import(filepath: &PathBuf) -> Vec<Card> {
    let mut reader = csv::Reader::from_path(filepath).unwrap();
    reader
        .deserialize::<Card>()
        .map(|item| item.unwrap())
        .filter(|card| card.status == "Not Owned" && !card.maybeboard)
        .collect::<Vec<Card>>()
}

impl Command for Ask {
    fn run(&self) -> Result<()> {
        let mut cube = import(&self.cube);
        cube.sort_by_key(|card| card.name.clone());
        println!("🧞 Your asking me for...");
        for card in cube {
            println!("1 {} ({})", card.name, card.Set);
        }
        Ok(())
    }
}

#[derive(Args)]
pub(crate) struct Grant {
    /// Path to the cube.
    cube: PathBuf,
}

#[derive(Parser)]
#[command()]
enum Base {
    /// Import list of cards from a CubeCobra CSV file.
    Ask(Ask),
}

impl Command for Base {
    fn run(&self) -> Result<()> {
        match self {
            Self::Ask(c) => c.run(),
        }
    }
}

fn main() -> Result<()> {
    Base::parse().run()
}
